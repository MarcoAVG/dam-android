package com.qualtop.am;

public class Configuration {
    private static FileLog fLog;


    public static void initLog() {
        fLog = new FileLog();
    }

    public static void writeLog(String slog){
        fLog.write(slog);
    }

    public static void closeLog() {
        fLog.close();
    }
}
