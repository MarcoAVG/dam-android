package com.qualtop.am;

import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.lang.reflect.Field;

import static com.qualtop.am.Configuration.closeLog;
import static com.qualtop.am.Configuration.initLog;
import static com.qualtop.am.Configuration.writeLog;


public class MainActivity extends ActionBarActivity {
    private String device_uuid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initLog();
        writeLog("os=" + getOS());
        device_uuid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        device_uuid = System.getProperty("http.agent");
        writeLog("UA=" + device_uuid);
        closeLog();
    }


    public String getOS(){
        String dat = "";
        try {
            StringBuilder builder = new StringBuilder();
            Field[] fields = Build.VERSION_CODES.class.getFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                int fieldValue = -1;

                try {
                    fieldValue = field.getInt(new Object());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                if (fieldValue == Build.VERSION.SDK_INT) {
                    builder.append(fieldName);
                }
            }
            dat = builder.toString();
            int in = dat.indexOf("_");
            if(in != -1)
                dat = dat.substring(0, dat.indexOf("_"));
        }catch (Exception es){

        }



        return dat;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
