package com.qualtop.am;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Marco AVG on 08/07/2015.
 */
public class FileLog {
    private static String pathLog ;
    private static PrintStream fileLog;
    static final String fullpath = "/sdcard/";
    static final String path = "data.txt";

    public FileLog() {
        try {
            System.out.println("The program is running...");
            String completepath = fullpath + path;
            System.out.println(completepath);
            fileLog = new PrintStream(completepath);//On this file will be record all messages
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileLog.class.getName()).log(Level.SEVERE, null, "Error in FileLog..." + ex.getMessage());
        } catch (NullPointerException ex) {
            Logger.getLogger(FileLog.class.getName()).log(Level.SEVERE, null, "Error in FileLog..." + ex.getMessage());
        }
    }
    public void write(String data) {
        try {
            fileLog.println(data);
            fileLog.flush();
        } catch(Exception ex) {
            Logger.getLogger(FileLog.class.getName()).log(Level.SEVERE, null, "Error in write..." + ex.getMessage());
        }
    }
    public void close(){
        try {
            fileLog.flush();
            fileLog.close();
        } catch(Exception ex) {
            Logger.getLogger(FileLog.class.getName()).log(Level.SEVERE, null, "Error in write..." + ex.getMessage());
        }
    }
}
